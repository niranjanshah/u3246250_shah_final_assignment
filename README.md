## Final Project: Data Science pipeline on-premises and on-the-cloud
### Author: Niranjan Shah
### StudentID: u3246250

### Introduction
This project focuses on Data Cleaning, EDA and model creation on-premise and on-the-cloud using the certified US air carriers dataset.
The project is separated into two notebooks.
1. onpremises.ipynb : This notebook contains all the necessary tasks required to run on premise. It performs the following tasks:
   * Unzip the compressed files that contains the dataset
   * Combine all the separate files into a single csv file
   * Cleaning the data to remove missing values
   * Exploratory Data Analysis
   * Feature engineering to add holiday and weather data
   * Saving the cleaned data to be used later on for dashboard and oncloud training
   * Develop models for predicting whether the flight is delayed or not
   * Provide performance metrics for the models
2. oncloud.ipynb : This notebook contains all the necessary taks required to train the model on cloud. It performs the following tasks:
   * This file needs to be uploaded to jupyterlab instance in Amazon SageMaker along with the generated csv files `combined_csv_v1.csv` and `combined_csv_v2.csv`
   * Remove the na that are introduced while uploading the csv files to Amazon SageMaker
   * Create train-test-val split of 70-15-15
   * Develop LinearLearner and XGBoost models on the cloud using both csv files. In total 4 models will be generated.
   * Deploy each model on a seperate `ml.m4.xlarge` instance
   * Perform batch transform on each model to generate performance metrics
   * Report the performance metrics

### Setup
* Install the following libraries using pip in python:
    * matplotlib
    * pandas
    * numpy
    * seaborn
    * zipfile
    * pathlib2
    * scikit-learn
* The `onpremises.ipynb` file can be opened in jupyter notebook, and each cell can be run sequentially to generate the results.
    * Create a `data` folder in the root directory of the project to store all the data downloaded and that will be generated later on.
    * Dataset needs to be downloaded and unzipped inside data folder of root. All the unzipped files will be zips, which will be handled in `onpremises.ipynb` to generate csv files
* The `oncloud.ipynb` file needs to be uploaded to Amazon Sagemaker along with the csv files generated.
    * After the upload is complete, you can run each cells in `oncloud.ipynb` sequentially to train models and see their performance.
    * The training of models takes quite a while due to network problems that might occur, which will disconnect the jupyter notebook.